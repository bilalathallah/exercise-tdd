import React from 'react'

export default function Counter() {
    const [counter, setCounter] = React.useState(0)
    return (
        <div>
            <h1>This is counter app</h1>
            <h2>Kelompok01 - Bilal</h2>
            <div id="counter-value">{counter}</div>
            <button id="increment-btn" onClick={() => setCounter(counter + 1)}>
                Increment
            </button>
            <button id="decrement-btn" onClick={() => setCounter(counter - 1)}>
                Decrement
            </button>
            <br />
            <button onClick={() => counter > 0 ? setCounter(counter - 1) : null} id="decrement-btn2">
                Decrement Above 0
            </button>
        </div>
    )
}